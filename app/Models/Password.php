<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelUserActivity\Traits\Loggable;
use Rappasoft\LaravelAuthenticationLog\Traits\AuthenticationLoggable;
use Illuminate\Notifications\Notifiable;

class Password extends Model
{
    use HasFactory;
    use Loggable, AuthenticationLoggable;
    protected $guarded  = [];
    public function scopeActive( $query) 
    {
        return $query->where('active', 1);
    }

    protected $fillable = [
        'address',
        'user',
        'pass',
        'type',
        'remark',
        'branch_code',
        'current_team_id',
        'active' 
    ];  
}
