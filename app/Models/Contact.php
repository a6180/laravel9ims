<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelUserActivity\Traits\Loggable;
use Illuminate\Notifications\Notifiable;
use Rappasoft\LaravelAuthenticationLog\Traits\AuthenticationLoggable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Contact extends Model
{
    use HasFactory;
    use Loggable;
    use Notifiable, AuthenticationLoggable;
    protected $guarded  = [];
     protected $fillable = [
        'renewal_date',
        'namedisplay',
        'group',
        'dept',
        'lantai',
        'extnumber',
        'digital',
        'remark',
        'current_team_id',
        'active'

         
    ];  
}
